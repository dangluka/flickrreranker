package com.example.flickrreranker.domain

import kotlin.properties.Delegates

data class PhotoRanking(
    val score: Double,
    val authorScore: Double,
    val viewsScore: Double,
    val geoScore: Double,
    val dateScore: Double,
) {
    var rank by Delegates.notNull<Int>()
    var previousRank by Delegates.notNull<Int>()
}