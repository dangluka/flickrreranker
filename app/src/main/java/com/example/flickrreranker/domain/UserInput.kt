package com.example.flickrreranker.domain

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserInput(
    val query: String,
    val count: Int,
    val author: String,
    val authorImportance: Double,
    val authorEnabled: Boolean,
    val views: Int,
    val viewsImportance: Double,
    val viewsEnabled: Boolean,
    val geo: LatLng,
    val geoImportance: Double,
    val geoEnabled: Boolean,
    val date: Long,
    val dateImportance: Double,
    val dateEnabled: Boolean,
) : Parcelable