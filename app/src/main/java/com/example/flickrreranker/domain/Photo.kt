package com.example.flickrreranker.domain

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

@Parcelize
data class Photo(
    val id: Long,
    val imgSrcUrl: String,
    val author: String,
    val views: Int,
    val geo: LatLng,
    val date: Long,
    val ranking: @RawValue PhotoRanking,
) : Parcelable