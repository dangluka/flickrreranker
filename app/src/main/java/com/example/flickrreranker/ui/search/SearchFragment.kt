package com.example.flickrreranker.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.flickrreranker.R
import com.example.flickrreranker.databinding.FragmentSearchBinding
import com.example.flickrreranker.domain.UserInput
import com.example.flickrreranker.ui.dateStringToUnix
import com.example.flickrreranker.ui.unixMillisToDateString
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointBackward
import com.google.android.material.datepicker.MaterialDatePicker

private const val KEYWORDS_KEY = "keywords"
private const val COUNT_KEY = "keywords"
private const val AUTHOR_KEY = "keywords"
private const val AUTHOR_ENABLED_KEY = "keywords"
private const val AUTHOR_IMPORTANCE_KEY = "keywords"
private const val VIEW_KEY = "keywords"
private const val VIEW_ENABLED_KEY = "keywords"
private const val VIEW_IMPORTANCE_KEY = "keywords"
private const val LATITUDE_KEY = "keywords"
private const val LONGITUDE_KEY = "keywords"
private const val MAP_ENABLED_KEY = "keywords"
private const val MAP_IMPORTANCE_KEY = "keywords"
private const val DATE_KEY = "keywords"
private const val DATE_ENABLED_KEY = "keywords"
private const val DATE_IMPORTANCE_KEY = "keywords"

class SearchFragment : Fragment(), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private var marker: Marker? = null

    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null) {
            binding.keywordsInput.setText(savedInstanceState.getString(KEYWORDS_KEY))
            binding.countInput.setText(savedInstanceState.getInt(COUNT_KEY))
            binding.authorInput.setText(savedInstanceState.getString(AUTHOR_KEY))
            binding.sliderAuthor.value = savedInstanceState.getFloat(AUTHOR_IMPORTANCE_KEY)
            binding.switchAuthor.isChecked = savedInstanceState.getBoolean(AUTHOR_ENABLED_KEY)
            binding.viewsInput.setText(savedInstanceState.getString(VIEW_KEY))
            binding.sliderViews.value = savedInstanceState.getFloat(VIEW_IMPORTANCE_KEY)
            binding.switchViews.isChecked = savedInstanceState.getBoolean(VIEW_ENABLED_KEY)
            binding.mapLatitudeInput.setText(savedInstanceState.getString(LATITUDE_KEY))
            binding.mapLongitudeInput.setText(savedInstanceState.getString(LONGITUDE_KEY))
            binding.sliderMap.value = savedInstanceState.getFloat(MAP_IMPORTANCE_KEY)
            binding.switchMap.isChecked = savedInstanceState.getBoolean(MAP_ENABLED_KEY)
            binding.dateInput.setText(savedInstanceState.getString(DATE_KEY))
            binding.sliderDate.value = savedInstanceState.getFloat(DATE_IMPORTANCE_KEY)
            binding.switchDate.isChecked = savedInstanceState.getBoolean(VIEW_ENABLED_KEY)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(KEYWORDS_KEY, binding.keywordsInput.text.toString())
        outState.putString(COUNT_KEY, binding.countInput.text.toString())
        outState.putString(AUTHOR_KEY, binding.authorInput.text.toString())
        outState.putFloat(AUTHOR_IMPORTANCE_KEY, binding.sliderAuthor.value)
        outState.putBoolean(AUTHOR_ENABLED_KEY, binding.switchAuthor.isChecked)
        outState.putString(VIEW_KEY, binding.viewsInput.text.toString())
        outState.putFloat(VIEW_IMPORTANCE_KEY, binding.sliderViews.value)
        outState.putBoolean(VIEW_ENABLED_KEY, binding.switchViews.isChecked)
        outState.putString(LATITUDE_KEY, binding.mapLatitudeInput.text.toString())
        outState.putString(LONGITUDE_KEY, binding.mapLongitudeInput.text.toString())
        outState.putFloat(MAP_IMPORTANCE_KEY, binding.sliderMap.value)
        outState.putBoolean(MAP_ENABLED_KEY, binding.switchMap.isChecked)
        outState.putString(VIEW_KEY, binding.viewsInput.text.toString())
        outState.putFloat(VIEW_IMPORTANCE_KEY, binding.sliderViews.value)
        outState.putBoolean(VIEW_ENABLED_KEY, binding.switchViews.isChecked)
        outState.putString(DATE_KEY, binding.dateInput.text.toString())
        outState.putFloat(DATE_IMPORTANCE_KEY, binding.sliderDate.value)
        outState.putBoolean(DATE_ENABLED_KEY, binding.switchDate.isChecked)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentSearchBinding.inflate(inflater)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupDatePicker()
        setupSubmitButton()
    }

    private fun setupSubmitButton() {
        binding.submitButton.setOnClickListener {
            try {
                val authorEnabled = binding.switchAuthor.isChecked
                val viewsEnabled = binding.switchViews.isChecked
                val geoEnabled = binding.switchMap.isChecked
                val dateEnabled = binding.switchDate.isChecked

                val userInput = UserInput(
                    binding.keywordsInput.text.toString(),
                    binding.countInput.text.toString().toInt(),
                    if (authorEnabled) binding.authorInput.text.toString() else "",
                    if (authorEnabled) binding.sliderAuthor.value.toDouble() else 0.0,
                    authorEnabled,
                    if (viewsEnabled) binding.viewsInput.text.toString().toInt() else 0,
                    if (viewsEnabled) binding.sliderViews.value.toDouble() else 0.0,
                    viewsEnabled,
                    if (geoEnabled) LatLng(binding.mapLatitudeInput.text.toString().toDouble(),
                        binding.mapLongitudeInput.text.toString().toDouble()) else LatLng(0.0, 0.0),
                    if (geoEnabled) binding.sliderMap.value.toDouble() else 0.0,
                    geoEnabled,
                    if (dateEnabled) binding.dateInput.text.toString().dateStringToUnix() else 0,
                    if (dateEnabled) 1.0 else 0.0,
                    dateEnabled)

                setFragmentResult("requestKey", bundleOf("userInput" to userInput))
                NavigationUI.navigateUp(findNavController(), null)
            } catch (e: NumberFormatException) {
                e.printStackTrace()
            }
        }
    }

    private fun setupDatePicker() {
        binding.dateInput.setOnClickListener { showDatePickerDialog() }
    }

    private fun showDatePickerDialog() {
        val datePicker = MaterialDatePicker.Builder.datePicker()
            .setTitleText("Select date")
            .setCalendarConstraints(
                CalendarConstraints.Builder()
                    .setValidator(DateValidatorPointBackward.now())
                    .build()
            )
            .build()

        datePicker.addOnPositiveButtonClickListener { unixMillis ->
            val dateInput = binding.dateInput
            val dateString = unixMillis.unixMillisToDateString()
            dateInput.setText(dateString)
        }

        datePicker.show(parentFragmentManager, null)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        val latitude = 37.422160
        val longitude = -122.084270
        val zoomLevel = 15f
        val startingLatLng = LatLng(latitude, longitude)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(startingLatLng, zoomLevel))

        enableMapScrollingInScrollView()
        setMapClick(map)
        map.uiSettings.isZoomControlsEnabled = true
    }

    private fun setMapClick(map: GoogleMap) {
        map.setOnMapClickListener { latLng ->
            marker?.remove()
            marker = map.addMarker(MarkerOptions().position(latLng))
            updateGeoText(latLng)
        }
    }

    private fun updateGeoText(latLng: LatLng) {
        binding.mapLatitudeInput.setText(latLng.latitude.toString())
        binding.mapLongitudeInput.setText(latLng.longitude.toString())
    }

    private fun enableMapScrollingInScrollView() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as? CustomMapFragment
        mapFragment?.listener = object : CustomMapFragment.OnTouchListener {
            override fun onTouch() {
                binding.scrollView.requestDisallowInterceptTouchEvent(true)
            }
        }
    }
}
