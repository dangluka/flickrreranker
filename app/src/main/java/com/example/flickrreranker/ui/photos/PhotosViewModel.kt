package com.example.flickrreranker.ui.photos

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.flickrreranker.data.PhotoRepository
import com.example.flickrreranker.domain.Photo
import com.example.flickrreranker.domain.UserInput
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PhotosViewModel(private val repository: PhotoRepository) : ViewModel() {

    private val _photos = MutableLiveData<MutableList<Photo>>()
    val photos: LiveData<MutableList<Photo>> = _photos

    private val _input = MutableLiveData<UserInput>()
    val input: LiveData<UserInput> = _input

    fun searchAndSort(userInput: UserInput) {
        _input.value = userInput
        viewModelScope.launch(Dispatchers.IO) {
            val photos = repository.searchPhotoEntities(userInput)
            photos.forEachIndexed { index, photo ->
                photo.ranking.previousRank = index + 1
            }
            photos.sortByDescending { it.ranking.score }
            photos.forEachIndexed { index, photo ->
                photo.ranking.rank = index + 1
            }
            _photos.postValue(photos)
        }
    }
}