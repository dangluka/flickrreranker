package com.example.flickrreranker.ui.photos

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import com.example.flickrreranker.R
import com.example.flickrreranker.databinding.FragmentPhotosBinding
import com.example.flickrreranker.domain.Photo
import com.example.flickrreranker.domain.UserInput
import com.google.android.gms.maps.model.LatLng
import org.koin.androidx.viewmodel.ext.android.viewModel

class PhotosFragment : Fragment() {

    private var _binding: FragmentPhotosBinding? = null
    private val binding get() = _binding!!

    private val viewModel: PhotosViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener("requestKey") { _, bundle ->
            val userInput = bundle.getParcelable<UserInput>("userInput")
            activity?.actionBar?.title = "\"${userInput?.query}\""
            viewModel.searchAndSort(userInput!!)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentPhotosBinding.inflate(inflater)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.photos.adapter = PhotosAdapter { photo -> onItemClicked(photo) }
//        test()
    }

    private fun test() {
        val userInput = UserInput(
            "cat",
            10,
            "Luke", 0.1, true,
            1000, 1.0, true,
            LatLng(1.0, 1.0), 0.1, true,
            1638088060, 0.1, true
        )
        viewModel.searchAndSort(userInput)
    }

    private fun onItemClicked(photo: Photo) {
        val userInput = viewModel.input.value
        val action =
            PhotosFragmentDirections.actionPhotoFragmentToPhotoDetailsFragment(photo, userInput!!)
        findNavController().navigate(action)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.photos_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
                navigateToSearch()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun navigateToSearch() {
        val action = PhotosFragmentDirections.actionPhotosFragmentToSearchFragment()
        findNavController().navigate(action)
    }
}