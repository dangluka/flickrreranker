package com.example.flickrreranker.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.flickrreranker.domain.Photo
import com.example.flickrreranker.domain.UserInput

class PhotoDetailsViewModel : ViewModel() {

    private val _photo = MutableLiveData<Photo>()
    val photo: LiveData<Photo> = _photo

    private val _input = MutableLiveData<UserInput>()
    val input: LiveData<UserInput> = _input

    fun loadPhoto(photo: Photo, userInput: UserInput) {
        _photo.value = photo
        _input.value = userInput
    }

}