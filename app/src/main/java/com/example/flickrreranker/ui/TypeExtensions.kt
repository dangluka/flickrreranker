package com.example.flickrreranker.ui

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*


fun Long.unixToDateString(): String =
    SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(this * 1000)

fun Long.unixMillisToDateString(): String =
    SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(this)

fun Boolean.toInt() = if (this) 1 else 0

fun String.dateStringToUnix(): Long {
    val l = LocalDate.parse(this, DateTimeFormatter.ofPattern("dd.MM.yyyy"))
    return l.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli() / 1000
}
