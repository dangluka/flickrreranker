package com.example.flickrreranker.data

import com.example.flickrreranker.domain.Photo
import com.example.flickrreranker.domain.PhotoRanking
import com.example.flickrreranker.domain.UserInput
import com.example.flickrreranker.domain.algorithms.Haversine
import com.example.flickrreranker.domain.algorithms.Levenshtein
import com.example.flickrreranker.framework.FlickrApi
import com.example.flickrreranker.framework.PhotoEntity
import com.example.flickrreranker.ui.toInt
import com.google.android.gms.maps.model.LatLng
import kotlin.math.abs

const val API_KEY = "7504831028e9440d377674f0ac06d76a"

class PhotoRepository(private val service: FlickrApi) {

    suspend fun searchPhotoEntities(userInput: UserInput): MutableList<Photo> =
        service.searchPhotos(
            apiKey = API_KEY,
            count = userInput.count,
            query = userInput.query
        ).photoEntities.photoEntities.toPhotos(userInput)
}

private fun List<PhotoEntity>.toPhotos(userInput: UserInput): MutableList<Photo> {
    return this.filter { it.imgSrcUrl != null }
        .map {
            Photo(it.id.toLong(),
                it.imgSrcUrl!!,
                it.author,
                it.views,
                LatLng(it.latitude, it.longitude),
                it.date,
                calculateRanking(it, this, userInput)
            )
        }.toMutableList()
}

fun calculateRanking(
    entity: PhotoEntity,
    entities: List<PhotoEntity>,
    userInput: UserInput,
): PhotoRanking {
    val parameterCount =
        userInput.authorEnabled.toInt()
    +userInput.viewsEnabled.toInt() +
            userInput.geoEnabled.toInt() +
            userInput.dateEnabled.toInt()
    val authorScore = calculateAuthorScore(entity, entities, userInput) / parameterCount
    val viewsScore = calculateViewsScore(entity, entities, userInput) / parameterCount
    val geoScore = calculateGeoScore(entity, entities, userInput) / parameterCount
    val dateScore = calculateDateScore(entity, entities, userInput) / parameterCount
    val overallScore = authorScore + viewsScore + geoScore + dateScore
    return PhotoRanking(overallScore, authorScore, viewsScore, geoScore, dateScore)
}

fun calculateAuthorScore(
    entity: PhotoEntity,
    entities: List<PhotoEntity>,
    userInput: UserInput,
): Double {
    if (!userInput.authorEnabled) return 0.0
    val absoluteDistance = Levenshtein.calc(entity.author, userInput.author)
    val entityWithMaxDistance =
        entities.maxByOrNull { Levenshtein.calc(it.author, userInput.author) }!!
    val maxDistance = Levenshtein.calc(entityWithMaxDistance.author, userInput.author)
    return (1 - absoluteDistance.toDouble() / maxDistance) * userInput.authorImportance
}

fun calculateViewsScore(
    entity: PhotoEntity,
    entities: List<PhotoEntity>,
    userInput: UserInput,
): Double {
    if (!userInput.viewsEnabled) return 0.0
    val absoluteDistance = abs(entity.views - userInput.views)
    val entityWithMaxDistance = entities.maxByOrNull { abs(it.views - userInput.views) }!!
    val maxDistance = abs(entityWithMaxDistance.views - userInput.views)
    return (1 - absoluteDistance.toDouble() / maxDistance) * userInput.viewsImportance
}

fun calculateGeoScore(
    entity: PhotoEntity,
    entities: List<PhotoEntity>,
    userInput: UserInput,
): Double {
    if (!userInput.geoEnabled) return 0.0
    val absoluteDistance = Haversine.calc(entity.latitude, entity.longitude,
        userInput.geo.latitude, userInput.geo.longitude)
    val entityWithMaxDistance =
        entities.maxByOrNull {
            Haversine.calc(it.latitude, it.longitude,
                userInput.geo.latitude, userInput.geo.longitude)
        }!!
    val maxDistance =
        Haversine.calc(entityWithMaxDistance.latitude, entityWithMaxDistance.longitude,
            userInput.geo.latitude, userInput.geo.longitude)
    return (1 - absoluteDistance / maxDistance) * userInput.geoImportance
}

fun calculateDateScore(
    entity: PhotoEntity,
    entities: List<PhotoEntity>,
    userInput: UserInput,
): Double {
    if (!userInput.dateEnabled) return 0.0
    val absoluteDistance = abs(entity.date - userInput.date)
    val entityWithMaxDistance = entities.maxByOrNull { abs(it.date - userInput.date) }!!
    val maxDistance = abs(entityWithMaxDistance.date - userInput.date)
    return (1 - absoluteDistance.toDouble() / maxDistance) * userInput.dateImportance
}
