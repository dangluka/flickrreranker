package com.example.flickrreranker.framework

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private const val BASE_API_URL = "https://www.flickr.com/services/"

interface FlickrApi {
    @GET("rest/?method=flickr.photos.search&format=json&nojsoncallback=1&page=1&per_page=5&extras=date_upload,%20owner_name,%20geo,%20views,%20url_c,%20license&page=1&has_geo=1&safe_search=1&content_type=1&privacy_filter=1&license=10,9,4,6,3,2,1,5,7")
    suspend fun searchPhotos(
        @Query("api_key") apiKey: String,
        @Query("per_page") count: Int,
        @Query("text") query: String
    ): PhotosResponseEntity
}

object PhotoApi {
    val service: FlickrApi by lazy {
        retrofit.create(FlickrApi::class.java)
    }
}

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

fun provideOkHttpClient(): OkHttpClient {
    val logging = HttpLoggingInterceptor()
    logging.setLevel(HttpLoggingInterceptor.Level.BASIC)
    return OkHttpClient().newBuilder()
        .addInterceptor(logging)
        .build()
}

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_API_URL)
    .client(provideOkHttpClient())
    .build()