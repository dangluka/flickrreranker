package com.example.flickrreranker.framework

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PhotoEntities(
    @Json(name = "photo") val photoEntities: List<PhotoEntity>,
)