package com.example.flickrreranker.framework

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PhotoEntity(
    @Json(name = "id") val id: String,
    @Json(name = "ownername") val author: String,
    @Json(name = "dateupload") val date: Long,
    @Json(name = "views") val views: Int,
    @Json(name = "latitude") val latitude: Double,
    @Json(name = "longitude") val longitude: Double,
    @Json(name = "url_l") val url_l: String?,
    @Json(name = "url_o") val url_o: String?,
    @Json(name = "url_c") val url_c: String?,
    @Json(name = "url_z") val url_z: String?,
    @Json(name = "url_n") val url_n: String?,
    @Json(name = "url_m") val url_m: String?,
    @Json(name = "url_q") val url_q: String?,
    @Json(name = "url_s") val url_s: String?,
    @Json(name = "url_t") val url_t: String?,
    @Json(name = "url_sq") val url_sq: String?,
) {
    var imgSrcUrl = url()
    private fun url(): String? {
        return url_l ?: url_o ?: url_c ?: url_z ?: url_n ?: url_m
        ?: url_q ?: url_s ?: url_t ?: url_sq
    }
}