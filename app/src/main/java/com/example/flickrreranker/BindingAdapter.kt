package com.example.flickrreranker

import android.annotation.SuppressLint
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.flickrreranker.domain.Photo
import com.example.flickrreranker.ui.photos.PhotosAdapter
import com.example.flickrreranker.ui.unixToDateString

@SuppressLint("NotifyDataSetChanged")
@BindingAdapter("listData")
fun bindPhotoRecyclerView(recyclerView: RecyclerView, data: List<Photo>?) {
    val adapter = recyclerView.adapter
    if (adapter is PhotosAdapter) {
        with(adapter) {
            submitList(data)
            notifyDataSetChanged()
        }
    }
}

@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        imgView.load(imgUrl) {
            placeholder(R.drawable.ic_loading_img)
            error(R.drawable.ic_broken_img)
        }
    }
}

@BindingAdapter("dateText")
fun bindDate(textView: TextView, unixMillis: Long?) {
    val datePlaceholder = textView.resources.getString(R.string.date)
    textView.text = datePlaceholder.format(unixMillis?.unixToDateString())
}

@BindingAdapter(value = ["bind:dateImportance", "bind:dateScore", "bind:dateString"])
fun bindDateInfo(textView: TextView, importance: Double?, score: Double?, unix: Long?) {
    val datePlaceholder = textView.resources.getString(R.string.date_info)
    textView.text = datePlaceholder.format(unix?.unixToDateString(), importance, score)
}